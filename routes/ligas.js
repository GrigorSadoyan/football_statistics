var express = require('express');
var router = express.Router();
var sha1 = require('sha1');
var query = require('../model/dbQuerys/querys');
var multer = require('multer');
var upload = multer();
const fs = require('fs');


upload.storage = multer.diskStorage({
    destination: './public/images/ligas/',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});


router.get('/', function(req, res, next) {
    query.findAllSortByOrd('all_ligas','ASC', function (result) {
        res.render('Ligas/ligas', { data: result });
    });
});

router.get('/add', function(req, res, next) {
        res.render('Ligas/ligasItem', {});
});


var logosImages = upload.fields([
        { name: 'image', maxCount: 1 }
    ]);

router.post('/add',logosImages, function(req, res, next) {
    if(req.body.name_en !== "" && req.body.name_ru !== "" && req.files['image'][0].filename.length > 0){
        var insertInfo = {
            name_en: req.body.name_en,
            name_ru: req.body.name_ru,
            image: req.files['image'][0].filename
        };
        console.log('insertInfo',insertInfo)
        query.insert2v('all_ligas', insertInfo, function (insertResult) {
            if (!insertResult) {
                res.redirect('/ligas');
            } else {
                console.log('insert chi exel');
            }
        })
    }else{
        res.end();
    }
});


router.get('/edit/:id', function(req, res, next) {
    query.findById('all_ligas',req.params.id, function (result) {
        console.log('result',result)
        res.render('Ligas/ligasItem', { data: result });
    });
});


var updateUploads = upload.fields([
    { name: 'image', maxCount: 1 }
]);

router.post('/edit/:id',updateUploads, function(req, res, next) {
    console.log('edit');
    console.log('req.body',req.body)
    console.log('req.files',req.files)
    query.findById('all_ligas',req.params.id, function (imageResults) {
        console.log('imageResults',imageResults);
        console.log('req.files',req.files);
        if (typeof req.files['image'] !== 'undefined') {
            if (req.files['image'][0].filename.length > 0) {
                fs.unlink('./public/images/ligas/' + imageResults[0]['image'] + '', function (err) {

                });
            }
        }
        var updateData =
            {
                values: {
                    name_en:req.body.name_en,
                    name_ru:req.body.name_ru,
                    image:typeof req.files['image'] !== 'undefined' ? req.files['image'][0].filename : imageResults[0]['image']
                },
                where: {
                    id:req.params.id
                }
            };
        query.update2v('all_ligas', updateData, function (success, result) {
            console.log('success',success);
            console.log('result',result);
            if(!success){
                res.redirect('/ligas');
            }else{
                console.log('YYYY');
                res.end();
            }
        })
    });
});

router.post('/sort/', function(req, res, next) {
    sortPortfolio(req.body.ords)
});


function sortPortfolio(data){
    var allData = data.split(",");
    for(var i = 0; i < allData.length; i++){
        var idVal = allData[i].split('_');
        var updateData =
            {
                values: {
                    ord: allData.indexOf(allData[i])
                },
                where: {
                    id: idVal[1]
                }
            };
        query.update2v('all_ligas', updateData, function (success, result) {

        })
    }
}

module.exports = router;
