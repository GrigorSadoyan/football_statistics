var express = require('express');
var router = express.Router();
var sha1 = require('sha1');
var query = require('../model/dbQuerys/querys');
var multer = require('multer');
var upload = multer();
const fs = require('fs');


upload.storage = multer.diskStorage({
    destination: './public/images/teams/',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});


router.get('/', function(req, res, next) {
    query.findAllSortByOrd('teams','ASC', function (result) {
        res.render('Teams/teams', { data: result });
    });
});


router.get('/add', function(req, res, next) {
    query.findAllSortByOrd('all_ligas','ASC', function (ligas) {
        res.render('Teams/teamsItem', { allLeague: ligas, data:undefined });
    });
});

var logosImages = upload.fields([
    { name: 'image', maxCount: 1 }
]);

router.post('/add',logosImages, function(req, res, next) {
    if(req.body.name_en !== "" && req.body.name_ru !== "" && req.files['image'][0].filename.length > 0 && req.body.league.length > 0){
        var insertInfo = {
            name_en: req.body.name_en,
            name_ru: req.body.name_ru,
            image: req.files['image'][0].filename
        };

        query.insert2v('teams', insertInfo, function (insertError,insertResult) {
            console.log('insertError',insertError);
            console.log('insertResult',insertResult);
            console.log('req.body.league',req.body.league);

            if (!insertError) {
                var filds = ['tid','lid'];
                var resInsert = [];
                for(var i = 0; i < req.body.league.length; i++){
                    resInsert.push([insertResult.insertId,req.body.league[i]])
                }
                console.log('resInsert',resInsert)
                query.insertLoop2v('teams_in_ligas', filds, resInsert, function (insertTeamsInLigasError) {
                    res.redirect('/teams');
                });
            } else {
                console.log('insert chi exel');
            }
        })
    }else{
        console.log('XXXX');
        res.end();
    }
});


router.get('/edit/:id', function(req, res, next) {
    query.findById('teams',req.params.id, function (resultTeam) {
        query.findAllSortByOrd('all_ligas','ASC', function (ligas) {
            var params = {tid:resultTeam[0].id}
            var filds = ['lid'];
            query.findByMultyName2v('teams_in_ligas',params,filds, function (errorTeamLigas,resultTeamLigas) {
                var thisTeamLegas = [];
                for(var i = 0; i < resultTeamLigas.length; i++){
                    thisTeamLegas.push(resultTeamLigas[i].lid)
                }
                res.render('Teams/teamsItem', { allLeague: ligas, data:resultTeam, thisTeamLigas : thisTeamLegas });
            });
        });
    });
});

var updateUploads = upload.fields([
    { name: 'image', maxCount: 1 }
]);

router.post('/edit/:id',updateUploads, function(req, res, next) {
    // console.log('edit');
    // console.log('req.body',req.body)
    // console.log('req.files',req.files)
    // return false
    query.findById('teams',req.params.id, function (imageResults) {
        console.log('imageResults',imageResults);
        console.log('req.files',req.files);
        if (typeof req.files['image'] !== 'undefined') {
            if (req.files['image'][0].filename.length > 0) {
                fs.unlink('./public/images/teams/' + imageResults[0]['image'] + '', function (err) {

                });
            }
        }
        var updateData =
            {
                values: {
                    name_en:req.body.name_en,
                    name_ru:req.body.name_ru,
                    image:typeof req.files['image'] !== 'undefined' ? req.files['image'][0].filename : imageResults[0]['image']
                },
                where: {
                    id:req.params.id
                }
            };
        query.update2v('teams', updateData, function (error, result) {
            console.log('error  Update',error);
            console.log('result',result);

            if(!error){
                query.deletes('teams_in_ligas',{tid:req.params.id}, function (error, resultDelete) {
                    if(!error) {
                        console.log('error Delete',error);
                        var filds = ['tid', 'lid'];
                        var resInsert = [];
                        for (var i = 0; i < req.body.league.length; i++) {
                            resInsert.push([req.params.id, req.body.league[i]])
                        }
                        query.insertLoop2v('teams_in_ligas', filds, resInsert, function (insertTeamsInLigasError) {
                            console.log('insertTeamsInLigasError',insertTeamsInLigasError);
                            res.redirect('/teams');
                        });
                    }
                })
            }else{
                console.log('YYYY');
                res.end();
            }
        })
    });
});

router.post('/sort/', function(req, res, next) {
    sortPortfolio(req.body.ords)
});


function sortPortfolio(data){
    var allData = data.split(",");
    for(var i = 0; i < allData.length; i++){
        var idVal = allData[i].split('_');
        var updateData =
            {
                values: {
                    ord: allData.indexOf(allData[i])
                },
                where: {
                    id: idVal[1]
                }
            };
        query.update2v('teams', updateData, function (success, result) {

        })
    }
}

module.exports = router;
