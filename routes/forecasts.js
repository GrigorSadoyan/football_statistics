var express = require('express');
var router = express.Router();
var query = require('../model/dbQuerys/querys');
var methods = require('../model/staticMethods/methods');

router.get('/', function(req, res, next) {
    query.findAllSortByOrd('forecasts','ASC', function (result) {
        var now = query.getTodayDate();
        res.render('Forecasts/forecasts', { data: result, todayDate : now.today });
    });
});

router.get('/add', function(req, res, next) {
    query.findAllSortByOrd('all_ligas','ASC', function (ligas) {
        res.render('Forecasts/forecastsItem', {allLigas:ligas});

    });
});

router.get('/edit/:id', function(req, res, next) {
    var now = query.getTodayDate();
    query.findForecast(req.params.id, function (forecast) {
        query.findForecastItems(req.params.id, function (forecastItems) {
            query.findAllSortByOrd('all_ligas','ASC', function (ligas) {
                query.selectAllTeamsByLigaId(forecast[0].lid, function (teamsByLiga) {
                    res.render('Forecasts/forecastsItemEdit', {
                        data:forecast,
                        forecastItems:forecastItems,
                        allLigas:ligas,
                        teams:teamsByLiga.data,
                        todayDate : now.today
                    });
                });
            });
        });
    });
});


router.post('/teams/select', function(req, res, next) {
    query.selectAllTeamsByLigaId(req.body.lId, function (teamsByLiga) {
        if(!teamsByLiga.error){
            res.json({error:false,data:teamsByLiga});
        }else{
            res.json({error:true,data:[]});
        }

    });
    // res.end();
});

router.post('/add', function(req, res, next) {

    var postData = req.body;
    // var postData =  { lid: '9',
    //     t1_id: '13',
    //     t2_id: '12',
    //     title_en: 'asd',
    //     title_ru: 'asd',
    //     desc_en: 'dsadsadasd ',
    //     desc_ru: 'sad',
    //     date_end: '2019-11-11',
    //     time_end: '23:11',
    //     game_props_en: 'asd',
    //     game_values_en: 'asd',
    //     game_props_ru: 'asdasd',
    //     game_values_ru: 'asdasd' };
    // console.log('postData',postData)

        if(postData.game_props_en.length != 0 &&
            postData.game_values_en.length != 0 &&
            postData.game_props_ru.length != 0 &&
            postData.game_values_ru.length != 0
        ){
            if(
                postData.t1_id != '' &&
                postData.t2_id != '' &&
                postData.title_en != '' &&
                postData.title_ru != '' &&
                postData.desc_en != '' &&
                postData.desc_ru != '' &&
                postData.lid != '' &&
                postData.date_end != '' &&
                postData.time_end != '' &&
                postData.t1_id != postData.t2_id
            ) {
                var allQuerys = [query.findByIdPromise('teams',postData.t1_id),query.findByIdPromise('teams',postData.t2_id)]
                Promise.all(allQuerys)
                    .then(function ([t1_name,t2_name]) {
                        var url = methods.generateUrlFromString(t1_name[0].name_en+' '+t2_name[0].name_en);
                        var insertData = {
                            'need_pay': postData.need_pay == 'on' ? '1' : '0',
                            't1_id': postData.t1_id,
                            't2_id': postData.t2_id,
                            'title_en': postData.title_en,
                            'title_ru': postData.title_ru,
                            'desc_en': postData.desc_en,
                            'desc_ru': postData.desc_ru,
                            'lid': postData.lid,
                            'date_end': postData.date_end,
                            'time_end': postData.time_end,
                            'url':url
                        }
                        console.log('insertData',insertData)
                        query.insert2v('forecasts', insertData, function (insertError, insertResult) {
                            if(!insertError){
                                var lastId = insertResult.insertId;
                                var filds = ['text_opt_en','text_opt_ru','text_res_en','text_res_ru','forecasts_id'];
                                var resInsert = [];
                                for(var i = 0;  i < postData.game_props_en.length; i++){
                                    resInsert.push(
                                        [
                                            postData.game_props_en[i],
                                            postData.game_props_ru[i],
                                            postData.game_values_en[i],
                                            postData.game_values_ru[i],
                                            insertResult.insertId
                                        ]
                                    )
                                }
                                query.insertLoop2v('forecasts_items', filds, resInsert, function (insertTeamsInLigasError) {
                                    res.redirect('/forecasts');
                                });
                            }
                        });
                    })
                    .catch(function () {
                        res.send('error # 13548631')
                    })
            }else{
                // qich lracrac toxer
                res.send('error - qich lracrac toxer kam nshvac en nuyn timere')
                console.log('qich lracrac toxer',req.body);
            }
        }else {
            res.send('qich nshac prognozner')
            console.log('qich nshac prognozner',req.body);
        }

    // console.log('...............',req.body);
});

router.post('/edit/:id', function(req, res, next) {
    var postData = req.body;
    if(postData.game_props_en.length != 0 &&
        postData.game_values_en.length != 0 &&
        postData.game_props_ru.length != 0 &&
        postData.game_values_ru.length != 0
    ) {
        if (
            postData.t1_id != '' &&
            postData.t2_id != '' &&
            postData.title_en != '' &&
            postData.title_ru != '' &&
            postData.desc_en != '' &&
            postData.desc_ru != '' &&
            postData.lid != '' &&
            postData.date_end != '' &&
            postData.time_end != '' &&
            postData.status != '' &&
            postData.score != '' &&
            postData.t1_id != postData.t2_id
        ) {
            query.deletes('forecasts_items',{forecasts_id:req.params.id}, function (errorDelete, resultDelete) {
                if(!errorDelete){
                    var allQuerys = [query.findByIdPromise('teams',postData.t1_id),query.findByIdPromise('teams',postData.t2_id)]
                    Promise.all(allQuerys)
                        .then(function ([t1_name,t2_name]) {
                            var url = methods.generateUrlFromString(t1_name[0].name_en+' '+t2_name[0].name_en);

                            var updateData =
                            {
                                values: {
                                    'need_pay': postData.need_pay == 'on' ? '1' : '0',
                                    'score': postData.score,
                                    't1_id': postData.t1_id,
                                    't2_id': postData.t2_id,
                                    'title_en': postData.title_en,
                                    'title_ru': postData.title_ru,
                                    'desc_en': postData.desc_en,
                                    'desc_ru': postData.desc_ru,
                                    'lid': postData.lid,
                                    'url': url,
                                    'date_end': postData.date_end,
                                    'time_end': postData.time_end
                                },
                                where: {
                                    id:req.params.id
                                }
                            };
                            var today = query.getTodayDate();
                            if(postData.date_end <= today.today){
                                updateData.values.status = postData.status == 'on' ? '1' : '0';
                            }else{
                                updateData.values.status = null;
                            }

                            query.update2v('forecasts', updateData, function (updateError, updateResult) {
                                if(!updateError){
                                    var filds = ['text_opt_en','text_opt_ru','text_res_en','text_res_ru','forecasts_id'];
                                    var resInsert = [];
                                    for(var i = 0;  i < postData.game_props_en.length; i++){
                                        resInsert.push(
                                            [
                                                postData.game_props_en[i],
                                                postData.game_props_ru[i],
                                                postData.game_values_en[i],
                                                postData.game_values_ru[i],
                                                req.params.id
                                            ]
                                        )
                                    }
                                    // console.log('resInsert',resInsert); return false
                                    query.insertLoop2v('forecasts_items', filds, resInsert, function (insertForecastItemsError) {
                                        if(!insertForecastItemsError){
                                            // 0 - Не Удачный
                                            // 1 - Удачный
                                            var allquery = [query.selectByStatus(0),query.selectByStatus(1)];
                                            Promise.all(allquery)
                                                .then(function ([UnSuccessful,Successful]) {
                                                    var UnSuccessfulCount = parseInt(UnSuccessful[0].counts);
                                                    var SuccessfulCount =  parseInt(Successful[0].counts);

                                                    var allCount = UnSuccessfulCount + SuccessfulCount;
                                                    var UnSuccessfulProcent = (UnSuccessfulCount * 100) / allCount;
                                                    var SuccessfulProcent = (SuccessfulCount * 100) / allCount;
                                                    console.log('UnSuccessfulProcent',UnSuccessfulProcent);
                                                    console.log('SuccessfulProcent',SuccessfulProcent);

                                                    var procentDatas =
                                                        {
                                                            values: {
                                                                'no': UnSuccessfulProcent,
                                                                'yes': SuccessfulProcent
                                                            },
                                                            where: {
                                                                id:1
                                                            }
                                                        };
                                                    query.update2v('status_procents', procentDatas, function (updateError, updateResult) {
                                                        if(!updateError){
                                                            res.redirect('/forecasts');
                                                        }else {
                                                            res.send('error , updates are not all')
                                                        }
                                                    });
                                                })
                                                .catch(function (error) {
                                                    res.send('error # 78626523')
                                                })

                                        }
                                    });
                                }
                            });
                    })
                    .catch(function () {
                        res.send('error # 213186163')
                    })
                }
            })
        }else{
            // qich lracrac toxer
            res.send('error - qich lracrac toxer kam nshvac en nuyn timere')
            console.log('qich lracrac toxer',req.body);
        }
    }else {
        // qich nshac prognozner
        console.log('qich nshac prognozner',req.body);
    }
});

router.post('/delete', function(req, res, next) {
    query.deletes('forecasts',{id:req.body.id}, function (errorDelete, resultDelete) {
        if(!errorDelete){
            res.json({error:false})
        }else {
            res.json({error:true})
        }
    });
});



router.post('/sort/', function(req, res, next) {
    sortPortfolio(req.body.ords)
});


function sortPortfolio(data){
    var allData = data.split(",");
    for(var i = 0; i < allData.length; i++){
        var idVal = allData[i].split('_');
        var updateData =
            {
                values: {
                    ord: allData.indexOf(allData[i])
                },
                where: {
                    id: idVal[1]
                }
            };
        query.update2v('forecasts', updateData, function (success, result) {

        })
    }
}

module.exports = router;
