var express = require('express');
var router = express.Router();
var query = require('../model/dbQuerys/querys');

router.get('/', function(req, res, next) {
    var allQuery = [query.findAllPromise('subscribings',null),query.findAllSortByOrdPromise('subscribings_rows','ASC')];
    Promise.all(allQuery)
        .then(function ([result,subscribings_rows]) {
            res.render('Subscribings/subscribings', {data : result, subscribingsRows:subscribings_rows});
        })
        .catch(function (error) {
            console.log('error',error)
        })
});

router.post('/:id', function(req, res, next) {
    var siD = req.params.id;
    var updateData =
        {
            values: {
                title_en: req.body.title_en != '' ? req.body.title_en : null,
                title_ru: req.body.title_ru != '' ? req.body.title_ru : null,
                label_en: req.body.label_en != '' ? req.body.label_en : null,
                label_ru: req.body.label_ru != '' ? req.body.label_ru : null,
                price: parseInt(req.body.price),
                active_days: parseInt(req.body.active_days)
            },
            where: {
                id:siD
            }
        };
    query.update2v('subscribings', updateData, function (success, result) {
        if(!success){
            res.redirect('/subscribings');
        }else{
            console.log('YYYY');
            res.end();
        }
    })
});

router.post('/add/row', function (req, res, next) {
    console.log('req.body',req.body)
    if(req.body.name_en !== '' && req.body.name_ru !== ''){
        var insertData = {
            name_en : req.body.name_en,
            name_ru : req.body.name_ru
        }
        console.log('insertData',insertData)
        query.insert2v('subscribings_rows',insertData, function (insertError, insertresult) {
            res.redirect('/subscribings');
        })
    }else{
        res.redirect('/subscribings');
    }
})

router.post('/delete/rows/', function (req, res, next) {
    console.log('req.body',req.body)
    // if(req.body.name_en !== '' && req.body.name_ru !== ''){
    //     var insertData = {
    //         name_en : req.body.name_en,
    //         name_ru : req.body.name_ru
    //     }
    //     console.log('insertData',insertData)
        query.deletes('subscribings_rows',{id:req.body.id}, function (error) {
            if(!error){
                res.json({error:false});
            }else{
                res.json({error:true});
            }

        })
    // }else{
    //     res.redirect('/subscribings');
    // }
})



router.post('/sort/orderings/', function(req, res, next) {
    sortPortfolio(req.body.ords)
});


function sortPortfolio(data){

    var allData = data.split(",");
    // allData.shift();
    console.log('allData',allData)
    // console.log('allData[0]',allData[0])
    for(var i = 0; i < allData.length; i++){
        var idVal = allData[i].split('_');
        var updateData =
            {
                values: {
                    ord: allData.indexOf(allData[i])
                },
                where: {
                    id: idVal[1]
                }
            };
        query.update2v('subscribings_rows', updateData, function (success, result) {
            console.log('success',success)
            console.log('result',result)
        })
    }
}

module.exports = router;
