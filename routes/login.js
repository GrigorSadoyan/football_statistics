var express = require('express');
var router = express.Router();
var sha1 = require('sha1');
var query = require('../model/dbQuerys/querys');

router.get('/', function(req, res, next) {
    res.render('login', { title: 'login' });

});

router.post('/', function(req, res, next) {
    if(req.body.login !== "" && req.body.password !== ""){
        var password = sha1(req.body.password);
        query.adminLogin({login: req.body.login, password: password}, function (result) {
            if(result.length === 1){
                res.cookie('admin_log', result[0].token);
                res.redirect('/');
            }
        })
    }else{
        console.log('XXXX');
        res.end();
    }
});


module.exports = router;
