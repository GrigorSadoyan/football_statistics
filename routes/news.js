var express = require('express');
var router = express.Router();
var sha1 = require('sha1');
var query = require('../model/dbQuerys/querys');
var multer = require('multer');
var upload = multer();
const fs = require('fs');


upload.storage = multer.diskStorage({
    destination: './public/images/news/',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});


router.get('/', function(req, res, next) {
    query.findAllSortByOrd('news','ASC', function (result) {
        // console.log("Nresult",result)
        res.render('News/news', { data: result });
    });
});

router.get('/add', function(req, res, next) {
    res.render('News/newsItem', {galleryResult : []});
});

// var newsImage = upload.fields([
//     { name: 'image', maxCount: 1 }
// ]);

var cpUploads = upload.fields([
    { name: 'image', maxCount: 1 }]);

router.post('/add',cpUploads, function(req, res, next) {
    if(req.body.title_en !== "" && req.body.title_ru !== "" &&  req.body.content_en !== "" && req.body.content_ru !== "" && req.files['image'][0].filename.length > 0){
        var insertInfo = {
            title_en: req.body.title_en,
            title_ru: req.body.title_ru,
            content_en: req.body.content_en,
            content_ru: req.body.content_ru,
            image: req.files['image'][0].filename
        };
        query.insert2v('news', insertInfo, function (insertResult) {
            if (!insertResult) {
                res.redirect('/news');
            } else {
                console.log('insert chi exel');
            }
        })
    }else{
        res.redirect('/news/add');
    }
});

router.get('/edit/:id', function(req, res, next) {
    query.findById('news',req.params.id, function (result) {
        console.log('result',result)
        res.render('News/newsItem', { data: result,galleryResult : [] });
    });
});


var updateUploads = upload.fields([
    { name: 'image', maxCount: 1 }
]);

router.post('/edit/:id',updateUploads, function(req, res, next) {
    query.findById('news',req.params.id, function (imageResults) {
        if (typeof req.files['image'] !== 'undefined') {
            if (req.files['image'][0].filename.length > 0) {
                fs.unlink('./public/images/news/' + imageResults[0]['image'] + '', function (err) {

                });
            }
        }
        var updateData =
            {
                values: {
                    title_en: req.body.title_en,
                    title_ru: req.body.title_ru,
                    content_en: req.body.content_en,
                    content_ru: req.body.content_ru,
                    image:typeof req.files['image'] !== 'undefined' ? req.files['image'][0].filename : imageResults[0]['image']
                },
                where: {
                    id:req.params.id
                }
            };
        query.update2v('news', updateData, function (success, result) {
            // console.log('success',success);
            // console.log('result',result);
            if(!success){
                res.redirect('/news');
            }else{
                console.log('YYYY');
                res.end();
            }
        })
    });
});


router.post('/sort/', function(req, res, next) {
    sortPortfolio(req.body.ords)
});


function sortPortfolio(data){
    var allData = data.split(",");
    for(var i = 0; i < allData.length; i++){
        var idVal = allData[i].split('_');
        var updateData =
            {
                values: {
                    ord: allData.indexOf(allData[i])
                },
                where: {
                    id: idVal[1]
                }
            };
        query.update2v('news', updateData, function (success, result) {

        })
    }
}


module.exports = router;
