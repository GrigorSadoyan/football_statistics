const db = require('../connection');


var adminLogin = function (params,cb) {
    var prepareSql = "SELECT * FROM admin_login WHERE `log` = '"+ params.login +"' AND `pass` = '"+ params.password +"'";
    db.query(prepareSql, function (err, rows, fields) {
        if (err) throw err;
        cb(rows)
    })
}

/*
 *   findAll => order { null || ( ASC || DESC )}
 */
var findAll = function(table,order,cb){
    var prepareSql = '';
    if(order == null){
        prepareSql = 'SELECT * FROM '+table+'';
    }else{
        prepareSql = 'SELECT * FROM '+table+' ORDER BY id '+ order +'';
    }
    db.query(prepareSql,function (err, rows, fields) {
        if (err) throw err;
        cb(rows)
    });
}

var findAllPromise = function(table,order){
    return new Promise(function (resolve, reject) {
        var prepareSql = '';
        if(order == null){
            prepareSql = 'SELECT * FROM '+table+'';
        }else{
            prepareSql = 'SELECT * FROM '+table+' ORDER BY id '+ order +'';
        }
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


var selectByStatus = function(type){
    return new Promise(function (resolve, reject) {

        var  prepareSql = 'SELECT COUNT(*) as counts FROM `forecasts` WHERE `status` = '+db.escape(type)+'';

        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


var findByIdPromise = function(table,id) {
    return new Promise(function (resolve, reject) {
        var prepareSql = "SELECT * FROM " + table + " WHERE `id`=" + db.escape(id);
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


var findAllShows = function(cb){
//     console.log('asdasd')
//     var prepareSql = 'SELECT `show_info`.`post_id`,`show_info`.`type_id`,`categories`.`id`,`categories`.`name_ru`,' +
//         ' `presentation`.`id` as presId,`presentation`.`title_ru`,`video`.`id` as vidId,`video`.`video_name_ru`,`infographics`.`id` as infoId,`infographics`.`image_name_ru`' +
//         ' FROM `show_info`' +
//         ' JOIN `categories` ON `show_info`.`type_id` = `categories`.`id`' +
//         ' IF(`show_info`.`type_id` = 1) then JOIN `presentation` ON `show_info`.`post_id` = `presentation`.`id` ' +
//         ' ELSEIF (`show_info`.`type_id` = 2) then JOIN `video` ON `show_info`.`post_id` = `video`.`id`'+
//         ' ELSEIF (`show_info`.`type_id` = 3) then JOIN `infographics` ON `show_info`.`post_id` = `infographics`.`id`';
//     console.log('.........................prepareSql',prepareSql)
//     db.query(prepareSql,function (err, rows, fields) {
//         if (err) throw err;
//         cb(rows)
//     });
}


/*
 *   findAll => order { null || ( ASC || DESC )}
 */
var findAllSortByOrd = function(table,order,cb){
    var prepareSql = '';



    var orderString = '';
    if(order == null){
        orderString = '';
    }else{
        orderString = 'ORDER BY ord '+ order +'';
    }
    prepareSql = 'SELECT * FROM `'+table+'` '+orderString;
    db.query(prepareSql,function (err, rows, fields) {
        if (err == null) {
            cb(rows)
        }else{
            cb('9999')
        }

    });
}



var findAllSortByOrdPromise = function(table,order,cb){
    return new Promise(function (resolve, reject) {
        var prepareSql = '';

        var orderString = '';
        if(order == null){
            orderString = '';
        }else{
            orderString = 'ORDER BY ord '+ order +'';
        }
        prepareSql = 'SELECT * FROM `'+table+'` '+orderString;
        db.query(prepareSql, function (error, rows, fields) {
            if (error){
                reject(error)
            }else{
                resolve(rows)
            }
        })
    })
}


var findForecast = function(id,cb){
    var prepareSql = '';

    prepareSql = "SELECT `forecasts`.*  " +
        "FROM `forecasts` " +
        "WHERE `forecasts`.`id`="+ db.escape(id);
    // console.log('prepareSql',prepareSql);
    db.query(prepareSql,function (err, rows, fields) {
        if (err == null) {
            cb(rows)
        }else{
            cb('9999')
        }

    });
}


var findForecastItems = function(id,cb){
    var prepareSql = '';

    prepareSql = "SELECT `forecasts_items`.*  " +
        "FROM `forecasts_items` " +
        "WHERE `forecasts_items`.`forecasts_id`="+ db.escape(id);
    // console.log('prepareSql',prepareSql);
    db.query(prepareSql,function (err, rows, fields) {
        if (err == null) {
            cb(rows)
        }else{
            cb('9999')
        }

    });
}

var findById = function(table,id,cb) {
    var prepareSql = "SELECT * FROM " + table + " WHERE `id`=" + db.escape(id);
    db.query(prepareSql, function (err, rows, fields) {
        if (err) throw err;
        cb(rows)
    })
}

var findTeamById = function(id,cb) {
    var prepareSql = "SELECT `teams`.`name_en`,`teams`.`name_ru`,`teams`.`image`," +
        "`teams_in_ligas`.`lid`," +
        "`all_ligas`.`name_en` as league_name " +
        "FROM `teams` " +
        "LEFT JOIN `teams_in_ligas` ON `teams_in_ligas`.`tid` = "+ db.escape(id)+" " +
        "LEFT JOIN `all_ligas` ON `all_ligas`.`id` =  `teams_in_ligas`.`lid` " +
        "WHERE `teams`.`id`=" + db.escape(id);
    db.query(prepareSql, function (err, rows, fields) {
        if (err) throw err;
        cb(rows)
    })
}


var selectAllTeamsByLigaId = function(id,cb) {
    var prepareSql = "SELECT `teams_in_ligas`.`tid` ,`teams_in_ligas`.`lid`, `teams`.`name_en` as `teams_name` " +
        "FROM `teams_in_ligas` " +
        "LEFT JOIN `teams` ON `teams`.`id` = `teams_in_ligas`.`tid` " +
        "WHERE `teams_in_ligas`.`lid`=" + db.escape(id);
    // console.log('prepareSql',prepareSql)
    db.query(prepareSql, function (err, rows, fields) {
        // if (err) throw err;
        if(err){
            cb({error:true,data:[]})
        }else{
            cb({error:false,data:rows})
        }

    })
}

var insert2v = function(table,post,cb){
    var prepareSql = 'INSERT INTO '+table+' SET ?';
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            // console.log('error',error)
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}


/**
 *
 * @param table
 * @param post {
 *              values: {fildname:value,.....}
 *              where: {fildname:value,.....}
 *          }
 * @param cb
 */

var update = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }
    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    // console.log('update',prepareSql);
    var query = db.query(prepareSql, post, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

var update2v = function(table,post,cb){
    var val = [];
    for(var key in post.values){
        val.push("`"+key +"`="+db.escape(post.values[key]));
    }
    var whereVal = [];
    for(var key in post.where){
        whereVal.push("`"+key+"`" +"="+db.escape(post.where[key]));
    }

    var prepareSql = "UPDATE "+ table + " SET "+val.toString()+" WHERE "+whereVal.join(' AND ')+"";
    // console.log('update',prepareSql);
    var query = db.query(prepareSql, function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}

/**
 *
 * @param table
 * @param params {fild_name:value}
 * @param filds [fild1,fild2]
 * @param cb function
 */
var findByMultyName2v = function (table,params,filds,cb) {
    var whereParams = [];
    for(var key in params){
        if(params[key] !== "NULL") {
            whereParams.push("`" + key + "`=" + db.escape(params[key]));
        }else{
            whereParams.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE '+whereParams.join(' AND ')+'';
    // console.log('prepareSql',prepareSql)
    db.query(prepareSql, function (err, rows, fields) {
        if (err){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:err.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}


var findByUrl = function (table,url,filds,cb) {

    var prepareSql = 'SELECT ' + filds.join(' , ') + ' FROM ' + table + ' WHERE `url_path` LIKE "'+url+'%"';
    // console.log('prepareSql',prepareSql)
    db.query(prepareSql, function (err, rows, fields) {
        if (err){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:err.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,rows)
        }
    });
}


/**
 *
 * @param table
 * @param post {fildname:value}
 * @param cb function(){}
 */
var deletes = function(table,post,cb){
    var val = [];
    for(var key in post){
        if(post[key] !== "NULL") {
            val.push("`" + key + "`=" + db.escape(post[key]));
        }else{
            val.push("`" + key + "` IS NULL");
        }
    }
    var prepareSql = 'DELETE FROM ' + table + ' WHERE '+val.join(' AND ')+'';
    //console.log(prepareSql);
    db.query(prepareSql,post,function(error,result,fields){
        if (error) throw error;
        cb(error,result)
    });
}


/**
 *
 * @param table
 * @param filds [fild1,fild2....]
 * @param post [fild1-val,fild2-val....]
 * @param cb   function(){}
 */

var insertLoop2v = function(table,filds,post,cb){
    var prepareSql = "INSERT INTO "+table+" ("+filds.join(',')+") VALUES  ?";
    var query = db.query(prepareSql, [post], function (error, results, fields) {
        if (error){
            var errorData = {
                code:error.code,
                errno:error.errno,
                sqlMessage:error.sqlMessage
            }
            cb(true,errorData);
        }else{
            cb(false,results)
        }
    });
}


function getTodayDate(){
    var result = {
        today:null,
        nextDay : null,
        nextTwoDay : null
    };

    var year  = new Date().getFullYear();
    var month = parseInt(new Date().getMonth());
    var day = parseInt(new Date().getDate());

    if(month >= 10)
        month = (month +1);
    if(month < 10)
        month = "0"+month;
    if(day < 10)
        day = "0"+day;
    // console.log('r.....................year',year   )
    // console.log('r.....................month',month)
    // console.log('r.....................day',day)
    result.today = year+"-"+month+"-"+day;
    // console.log('r......................result.today',result.today)

    var day = new Date();
    var nextDay = new Date(day);
    nextDay.setDate(day.getDate()+1);
    var nextYear  = nextDay.getFullYear();
    var nextMonth = nextDay.getMonth();
    var nextDays = nextDay.getDate();
    if(nextMonth >= 10)
        nextMonth = (nextMonth +1);
    if(nextMonth < 10)
        nextMonth = "0"+nextMonth;
    if(nextDays < 10)
        nextDays = "0"+nextDays;

    result.nextDay = nextYear+"-"+nextMonth+"-"+nextDays;


    var dayTwo = new Date();
    var nextDayTo = new Date(dayTwo);
    nextDayTo.setDate(dayTwo.getDate()+2);
    var nextYearTo  = nextDayTo.getFullYear();
    var nextMonthTwo = nextDayTo.getMonth();
    var nextDayTos = nextDayTo.getDate();
    if(nextMonthTwo >= 10)
        nextMonthTwo = (nextMonthTwo +1);
    if(nextMonthTwo < 10)
        nextMonthTwo = "0"+nextMonthTwo;
    if(nextDayTos < 10)
        nextDayTos = "0"+nextDayTos;
    result.nextTwoDay = nextYearTo+"-"+nextMonthTwo+"-"+nextDayTos;

    return result;

}

module.exports.adminLogin = adminLogin;
module.exports.selectByStatus = selectByStatus;
module.exports.findAll = findAll;
module.exports.findAllPromise = findAllPromise;
module.exports.findAllShows = findAllShows;
module.exports.findById = findById;
module.exports.insert2v = insert2v;
module.exports.insertLoop2v = insertLoop2v;
module.exports.update = update;
module.exports.update2v = update2v;
module.exports.findByMultyName2v = findByMultyName2v;
module.exports.findByUrl = findByUrl;
module.exports.findByIdPromise = findByIdPromise;
module.exports.deletes = deletes;
module.exports.findAllSortByOrd = findAllSortByOrd;
module.exports.findTeamById = findTeamById;
module.exports.selectAllTeamsByLigaId = selectAllTeamsByLigaId;
module.exports.findForecast = findForecast;
module.exports.findForecastItems = findForecastItems;
module.exports.findAllSortByOrdPromise = findAllSortByOrdPromise;
module.exports.getTodayDate = getTodayDate;
