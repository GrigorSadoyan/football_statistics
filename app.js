var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
// const bodyParser = require("body-parser");
// bodyParser.urlencoded({extended: false});

var app = express();

var indexRouter = require('./routes/index');
var loginRouter = require('./routes/login');
var ligasRouter = require('./routes/ligas');
var teamsRouter = require('./routes/teams');
var newsRouter = require('./routes/news');
var forecastsRouter = require('./routes/forecasts');
var ckimagesRouter = require('./routes/ckimages');
var subscribingsRouter = require('./routes/subscribings');
var aboutRouter = require('./routes/about');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// check Cookie exists or not
app.use(function(req, res, next) {
    var cookie = req.cookies['admin_log'];
    if(typeof cookie === 'undefined'){
        if(req.originalUrl === '/login'){
            next();
        }else{
            res.redirect('/login');
        }
    }else{
        next();
    }
});

app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/ligas', ligasRouter);
app.use('/teams', teamsRouter);
app.use('/news', newsRouter);
app.use('/forecasts', forecastsRouter);
app.use('/ckimages', ckimagesRouter);
app.use('/subscribings', subscribingsRouter);
app.use('/about', aboutRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
