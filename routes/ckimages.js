var express = require('express');
var router = express.Router();
var sha1 = require('sha1');
var query = require('../model/dbQuerys/querys');
var multer = require('multer');
var upload = multer();
const fs = require('fs');


upload.storage = multer.diskStorage({
    destination: './public/images/forck/',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});

var updateUploads = upload.fields([
    { name: 'img', maxCount: 1 }
]);

router.post('/images/forck/',updateUploads, function(req, res, next) {
    if(req.files['img'][0].filename.length > 0){
        var insertInfo = {image: req.files['img'][0].filename};
        query.insert2v('images_for_ck', insertInfo, function (insertResult, data) {
            if (!insertResult) {
                res.json({imgName : insertInfo.image});
            } else {
                console.log('data',data);
            }
        })
    }
});


module.exports = router;
